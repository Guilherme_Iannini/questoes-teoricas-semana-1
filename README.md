

##<b>Questões teóricas – Guilherme Iannini Dutra


1.           O que é Git?

**R**: git é um repositório de código, uma forma de armazenar os códigos criados, podendo ser armazenado tanto em um projeto git quanto na própria máquina.Geralmente se segue o padrão de desenvolvimento git flow


2.	          O que é a staging área ?

**R**: Staging é um dos ramos da branch utilizados no gitflow. É a área onde são testados as modificações ou criações de novas funções existentes no ramo develop.

3.          	O que é o working directory? 

**R**: tratasse de todos os arquivos que estiverem sendo trabalhados no momento


4.	            O que é um commit?

**R**:São as modificações adicionadas na staging area, que caso seja aprovadas em todos os testes, serão adicionadas no ramo master, alterando sua versão


5.	            O que é uma branch?

**R:** branch significa ramificação do código, é utilizado no git flow  como um ponteiro pra um commit e tudo anterior a esse commit 


6.            O que é o head no Git?

**R:** é um ponteiro que aponta para alguma branch ou algum commit


7.             O que é um merge?

**R:**  é a junção de dois gits diferentes provenientes de duas branchs diferentes



8.         Explique os 4 estados de um arquivo no Git.

**untracked** :Arquivos que não estavam no último commit;

**unmodified** :Arquivos não modificados desde o último commit;

**modified** :Arquivos modificados desde o último commit; e

**staged:**
 Arquivos preparados para comitar.



9.	          Explique o comando git init

**R:** cria um repositório, antes é necessário criar um diretório local para o código fonte do seu software

10.	       Explique o comando git add. 

**R:** o comando git add, altera o estado de um arquivo de código, quando o arquivo é adicionado em um repositório manualmente, seu estado é “untracked”, ou seja, arquivo que não estava no último commit. Após usar o comando “git add <nome_do_arquivo>”	
o arquivo irá mudar seu estado para staged, ou seja, um arquivo pronto para commitar.

11.	       Explique o comando git status. 

**R:**  o comando git status é utilizado para conferir se o arquivo foi adicionado na staging área


12.	       Explique o comando git commit

**R:** o git commit faz com que os arquivos do staging área sejam versionados, geralmente é adicionada uma mensagem a fim de deixar claro o que foi adicionado e facilitar a operação. Sintaxe:  git commit -m “Mensagem do commit”


13.	       Explique o comando git log. 

**R:** mostra o histórico de commits realizados no repositório


14.	       Explique o comando git checkout 

**R:** O comando git checkout permite navegar entre ramificações criadas pelo git branch.


15.	       Explique o comando git reset e suas três opções.

**R:** nesse caso temos 3 possíveis comandos que serão utilizados de acordo com a necessidade.

**-Soft:** opção soft, move o HEAD para o commit indicado, mas mantém o staging e o working directory inalterados

**-Mixed:** A opção mixed, move o HEAD para o commit indicado, altera o staging e mantém o working directory,

 **Hard:** A opção hard faz com que o HEAD aponte para algum commit anterior, mas também altera a staging área e o working directory para o estado do commit indicado, ou seja, todas as alterações realizadas após o commit ao qual retornamos serão perdidas. Isso não é recomendável se as alterações já tiverem sido enviadas para o repositório remoto. Nesse caso devemos utilizar o git revert.


16.	      Explique o comando git revert. 

**R:** cria um novo commit com as alterações do commit indicado.



17.	      Explique o comando git clone. 

**R:** baixa um repositório remoto para nossa máquina


18.	      Explique o comando git push. 

**R:** move as alterações feitas em nossa máquina para o repositório remoto. Sinxate:
“git push -u origin master”.



19.	      Explique o comando git pull. 

**R:** O comando git pull é usado para buscar e baixar conteúdo de repositórios remotos e fazer a atualização imediata ao repositório local para que os conteúdos sejam iguais


20.	      Como ignorar o versionamento de arquivos no Git? 

**R:** para ignorar o versionamento de arquivos, criamos um arquivo .gitignore e adicionamos todos os arquivos que não dever ser versionados através do comando echo 



**git checkout -b develop master**, que cria o ramo develop a partir do ramo master

**git checkout -b staging** develop que cria o ramo staging 

 **git push -u origin develop e git push -u origin staging** para subir as alterações no repositório remoto.

Utilizando o comando git branch podemos verificar os ramos em nosso repositório local. Para verificarmos os ramos remotos, utilizamos o comando **git branch -r.**


21.	       No terralab utilizamos as branches master ou main, develop e staging. Explique o objetivo de cada uma. 

**R:**  **Branch master:** é nosso arquivo fonte, que para ser alterado segue a dinâmica git flow

**Branch develop:** é criada a partir da branch master, é onde os arquivos podem ser editados ou novas funções criadas, sem alterar a branch master.Antes dessas alterações irem para a branch master, devem passar pela branch staging.

**Branch Staging:** é onde as alterações do branch develop são testadas. Se as alterações estiveram funcionando após os devidos testes, elas podem ser salvas na branch master através do commit, dessa forma a brach master é atualizada para um nova versão.

**Git branch:** se movimenta em direção a uma branch do repositório
**Git branch -b “develop”:** cria e se movimenta em direção a branch develop


